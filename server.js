const path = require("path");
const express = require("express");
const dotenv = require("dotenv");
const morgan = require("morgan");
const fileupload = require("express-fileupload");
const cookieParser = require("cookie-parser");
const mongoSanitize = require("express-mongo-sanitize");
const helmet = require("helmet");
const xss = require("xss-clean");
const rateLimit = require("express-rate-limit");
const hpp = require("hpp");

const cors = require("cors");
const errorHandler = require("./middleware/error");
const connectDB = require("./config/db");

// load env vars
dotenv.config({ path: "./config/config.env" });
// Connect to database
connectDB();
// Route files
const collectives = require("./routes/collectives");
const posts = require("./routes/posts");
const projects = require("./routes/projects");
const filefolders = require("./routes/filefolders");
const files = require("./routes/files");
const auth = require("./routes/auth");
const users = require("./routes/users");
const comments = require("./routes/comments");
const replies = require("./routes/replies");
const votes = require("./routes/votes");
const notifications = require("./routes/notifications");
const mods = require("./routes/mods");

const app = express();

// Add body parser via express
app.use(express.json({limit: '200mb'}));

// Cookie parser
app.use(cookieParser());

// dev logging middleware
// should only run if env is dev
process.env.NODE_ENV === "development" ? app.use(morgan("dev")) : null;

// File uploading
app.use(fileupload());

// Sanitize data
app.use(mongoSanitize());

// Set security headers
app.use(helmet());

// Prevent xss (cross side scripting) attacks
app.use(xss());

// // Rate limiting
// const limiter = rateLimit({
//   windowMs: 10 * 60 * 1000, // 10 minutes
//   max: 1000
// });
// app.use(limiter);

// Prevent http param pollution
app.use(hpp());

// Enable CORS
app.use(cors());

// Set static folder
app.use(express.static(path.join(__dirname, "public")));

// mount routers
app.use("/api/v1/collectives", collectives);
app.use("/api/v1/posts", posts);
app.use("/api/v1/projects", projects);
app.use("/api/v1/filefolders", filefolders);
app.use("/api/v1/files", files);
app.use("/api/v1/auth", auth);
app.use("/api/v1/users", users);
app.use("/api/v1/comments", comments);
app.use("/api/v1/replies", replies);
app.use("/api/vote-service/v1/votes", votes);
app.use("/api/v1/mods", mods);
app.use("/api/v1/notifications", notifications);
app.use(errorHandler);

// grab specified port and set to PORT var.
// if not available listen on 5000
const PORT = process.env.PORT || 5000;

const server = app.listen(
  PORT,
  console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`)
);

// handle unhandled promise rejections
process.on("unhandledRejection", (err, promise) => {
  console.log(`Error: ${err.message}`);
  // Close Server & exit process
  server.close(() => {
    // we want to exit with failure so pass a 1
    process.exit(1);
  });
});
