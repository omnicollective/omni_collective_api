const fs = require("fs");
const mongoose = require("mongoose");
const dotenv = require("dotenv");

// Load env vars
dotenv.config({ path: "./config/config.env" });

// Load models
const Collective = require("./models/Collective");
const Post = require("./models/Post");
const Project = require("./models/Project");
const FileFolder = require("./models/FileFolder");
const User = require("./models/User");
const Comment = require("./models/Comment");
const Mod = require("./models/Mod");
const Vote = require("./models/Vote");
const Notification = require("./models/Notification");

// Connect to DB
mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
});

// Read the JSON files
const collectives = JSON.parse(
  fs.readFileSync(`${__dirname}/_data/collectives.json`, "utf-8")
);
// const posts = JSON.parse(fs.readFileSync(`${__dirname}/_data/posts.json`, 'utf-8'));
const projects = JSON.parse(
  fs.readFileSync(`${__dirname}/_data/projects.json`, "utf-8")
);
const file_folders = JSON.parse(
  fs.readFileSync(`${__dirname}/_data/file_folders.json`, "utf-8")
);
const users = JSON.parse(
  fs.readFileSync(`${__dirname}/_data/users.json`, "utf-8")
);
// const comments = JSON.parse(fs.readFileSync(`${__dirname}/_data/comments.json`, 'utf-8'));

// Import into DB
const importData = async () => {
  try {
    await Collective.create(collectives);
    // await Post.create(posts);
    await Project.create(projects);
    await FileFolder.create(file_folders);
    // await Comment.create(comments);
    await User.create(users);

    console.log("Data Imported...");
    process.exit();
  } catch (err) {
    console.error(err);
  }
};

// Delete data
// Import into DB
const deleteData = async () => {
  try {
    // await Project.
    await Collective.deleteMany();
    await Post.deleteMany();
    await Mod.deleteMany();
    await Project.deleteMany();
    await FileFolder.deleteMany();
    await Comment.deleteMany();
    // await User.deleteMany();
    await Notification.deleteMany();
    await Vote.deleteMany();

    console.log("Data Destroyed...");
    process.exit();
  } catch (err) {
    console.error(err);
  }
};

if (process.argv[2] === "-i") {
  importData();
} else if (process.argv[2] === "-d") {
  deleteData();
}
