const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Collective = require("../models/Collective");
const User = require("../models/User");
const Mod = require("../models/Mod");


// @desc    Join a Collective
// @route   POST /api/v1/collectives/:id/mods
// @access  Private
exports.joinCollective = asyncHandler(async (req, res, next) => {
    req.body.collectiveID = req.params.collectiveId;

    const collective = await Collective.findById(req.body.collectiveID);

    if (!collective) {
      return next(
        new ErrorResponse(`The collective with ID ${req.body.collectiveID} does not exist`, 400)
      );
    }

    const alreadyExists = await Mod.findOne({ userID: req.user._id, collectiveID: req.body.collectiveID})
    if (alreadyExists) {
      return next(
        new ErrorResponse(`The user with ID ${req.user._id} already belongs to this collective`, 400)
      );
    }

    const joinCollective = await Mod.create(req.body);

    res.status(201).json({
      success: true,
      data: joinCollective,
    });
    
  
  });

// @desc    Leave a collective
// @route   DELETE /api/v1/collectives/:id/mods/:id
// @access  Private
exports.leaveCollective = asyncHandler(async (req, res, next) => {

    const collective = await Collective.findById(req.params.collectiveId);

    if (!collective) {
      return next(
        new ErrorResponse(`The collective with ID ${req.params.collectiveId} does not exist`, 400)
      );
    }
    
    // allow deletion from only the user
    const moderator = await Mod.findOne({ userID: req.user._id, collectiveID: req.params.collectiveId, _id: req.params.id})
    if (!moderator) {
        return next(
          new ErrorResponse(`A moderator with matching credentials could not be found`, 400)
        );
    }
  
    moderator.remove();
  
    res.status(200).json({ success: true, data: {} });
  });

// @desc    Get Mods
// @route   GET /api/v1/mods
// @route   GET /api/v1/collectives/:id/mods
// @access  Private
exports.getMods = asyncHandler(async (req, res, next) => {
  const users = await User.find();
  const collectives = await Collective.find();
//   const votes = await Vote.find();
  let mods;

  if (req.params.collectiveId) {

    const collective = await Collective.findById(req.params.collectiveId);

    if (!collective) {
      return next(
        new ErrorResponse(`The collective with ID ${req.params.collectiveId} does not exist`, 400)
      );
    }
    mods = await Mod.find({ collectiveID: req.params.collectiveId });
    const mods2 = JSON.parse(JSON.stringify(mods));
    const modMap = mods2.map(x => {
      let uid = x.userID;
      let cid = x.collectiveID;
      let mid = x.id;

      const userMap = users.filter(y => {
        if (uid === y.id) {
          return y.username;
        }
      });
      let userInfo = JSON.parse(JSON.stringify({data: userMap})).data[0];
      let username = userInfo.username ? userInfo.username : "placeholder"; 
      const cMap = collectives.filter(y => {
        if (cid === y.id) {
          return y.name;
        }
      });
      let cInfo = JSON.parse(JSON.stringify({data: cMap})).data[0];
      let collectiveName = cInfo.name;
      return {...x, username, collectiveName};
    });
    res.status(200).json({
      success: true,
      data: modMap,
    });
  } else {
    const mods2 = JSON.parse(JSON.stringify(res.advancedResults)).data;
    const modMap = mods2.map(x => {
      let uid = x.userID;
      let cid = x.collectiveID;
      let mid = x.id;

      const userMap = users.filter(y => {
        if (uid === y.id) {
          return y.username;
        }
      });
      let userInfo = JSON.parse(JSON.stringify({data: userMap})).data[0];
      let username = userInfo.username ? userInfo.username : "placeholder"; 
      const cMap = collectives.filter(y => {
        if (cid === y.id) {
          return y.name;
        }
      });
      let cInfo = JSON.parse(JSON.stringify({data: cMap})).data[0];
      let collectiveName = cInfo.name;
      return {...x, username, collectiveName};
    });
    res.status(200).json({
      success: true,
      data: modMap,
    });
  }
  });

// @desc    Update mod
// @route   PUT /api/v1/mods/:id
// @access  Private
exports.updateMod = asyncHandler(async (req, res, next) => {
  if (req.params.id !== '000') {
    let mod = await Mod.findById(req.params.id);
  
    if (!mod) {
      return next(
        new ErrorResponse(`No mod with the id of ${req.params.id}`, 404)
      );
    }
  
    if (mod.userID !== req.user.id) {
      return next(
        new ErrorResponse(
          `User ${req.user.id} is not authorized to update mod ${mod._id}`,
          401
        )
      );
    }
  
    mod = await Mod.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });
  
    res.status(200).json({
      success: true,
      data: mod,
    });
  }
  
  if (req.params.id === '000') {
    let mod = await Mod.updateMany({ userID: req.user.id }, req.body, {
      new: true,
      runValidators: true,
    });
    
    if (!mod) {
      return next(
        new ErrorResponse(`No mod with the id of ${req.params.id}`, 404)
      );
    }
  
    res.status(200).json({
      success: true,
      data: mod,
    });
  }
  
});

// @desc    Get mod By UID
// @route   PUT /api/v1/mods/:id
// @access  Private
exports.getModByUID = asyncHandler(async (req, res, next) => {
    const user = await User.findById(req.params.id);

    if (!user) {
      return next(
        new ErrorResponse(`Unable to find user existing user with ID ${req.params.id}`, 400)
      );
    }
    
    const mods = await Mod.find({ userID: req.params.id });

    if (!mods) {
      return next(
        new ErrorResponse(`Unable to find mods with existing user ID of ${req.params.id}`, 400)
      );
    }
    
    res.status(200).json({
      success: true,
      data: { mods },
    });
  
});