const User = require("../models/User");
const Notification = require("../models/Notification");
const Mod = require("../models/Mod");
const Comment = require("../models/Comment");
const Post = require("../models/Post");
const Vote = require("../models/Vote");
const path = require("path");
const asyncHandler = require("../middleware/async");
const {Storage} = require('@google-cloud/storage');
const storage = new Storage({
  keyFilename: process.env.GOOGLE_APPLICATION_CREDENTIALS,
});

async function uploadFile(filePath, fileName) {
  if (!filePath) return;

  var thumbBucket = storage.bucket("omni-thumbnails");
  // let success = false;
  var stream = require('stream');
  var bufferStream = new stream.PassThrough();
  try {
    bufferStream.end(Buffer.from(filePath, 'base64'));
  } catch (error) {
    console.log(error);
  }


  let file = thumbBucket.file(`${fileName}.png`);
  bufferStream.pipe(file.createWriteStream({
    metadata: {
      contentType: 'image/png',
      metadata: {
        custom: 'metadata'
      }
    },
    public: true,
    validation: "md5"
  }))
  .on('error', function(err) {
    console.log(err);
  })
  .on('finish', function() {
    // The file upload is complete.
    console.log("Upload Complete");
    // success = true;
  });
  

  // await storage.bucket("omni-thumbnails").upload(file, {
  //   destination: destFileName,
  // });

  console.log(`file uploaded to omni-thumbnails`);
  // let fileName = success ? destFileName : "";
  // return fileName;
}


uploadFile().catch(console.error);


// @desc    Get all users
// @route   GET /api/v1/users
// @access  Public
exports.getUsers = async (req, res, next) => {
  try {
    const users = await User.find();
    res.status(200).json({
      success: true,
      count: users.length,
      data: users,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
    });
  }
};

// @desc    Get single user
// @route   GET /api/v1/users/:id
// @access  Public
exports.getUser = async (req, res, next) => {
  try {
    const user = await User.findById(req.params.id);

    if (!user) {
      return res.status(400).json({ success: false });
    }

    res.status(200).json({
      success: true,
      data: user,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
    });
  }
};

// @desc    Create new user
// @route   User /api/v1/users
// @access  Private
exports.createUser = async (req, res, next) => {
  let body = {
    ...req.body,
    nickname: req.body.username,
  };
  try {
    const newUser = await User.create(req.body);

    res.status(201).json({
      success: true,
      data: newUser,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
    });
  }
};

// @desc    Update user
// @route   PUT /api/v1/users/:id
// @access  Private
exports.updateUser = async (req, res, next) => {

  let user = await User.findById(req.params.id);

  if (!user) {
    return res.status(400).json({ success: false });
  }

  try {
    const currentDate = new Date();
    const date = currentDate.getDate();
    const year = currentDate.getFullYear();
    const month = currentDate.getMonth();
    const destFileName = `${year}-${month}-${date}-${currentDate.getTime()}`;
  
    if (req.body.photo) {
      uploadFile(req.body.photo, destFileName);
      
    }
    console.log(destFileName);
  
    req.body.photo = destFileName;

    let userUpdated = await User.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });

    if (!userUpdated) {
      return res.status(400).json({
        success: false,
      });
    }

    res.status(200).json({
      success: true,
      data: userUpdated,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
    });
  }
};

// @desc    Delete user
// @route   DELETE /api/v1/users/:id
// @access  Private
exports.deleteUser = async (req, res, next) => {

  if (req.params.id === "000000000000000000000000") {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is unable to be deleted`,
        401
      )
    );
  }
  res.cookie("token", "none", {
    expires: new Date(Date.now() + 10 * 1000),
    httpOnly: true,
  });
  try {
    await Notification.deleteMany({ notifieeID: req.params.id });
    await Mod.deleteMany({ userID: req.params.id });
    await Vote.deleteMany({ userID : req.params.id });
    const body = {
      user: "000000000000000000000000",
    };
    await Comment.updateMany({ "user" : req.params.id }, body);
    await Post.updateMany({ "user" : req.params.id }, body);
    await User.findByIdAndDelete(req.params.id);


    res.status(200).json({ success: true, data: {} });
  } catch (error) {
    res.status(400).json({ success: false });
  }
};
