const path = require("path");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const Vote = require("../models/Vote");
const User = require("../models/User");
const Comment = require("../models/Comment");
const Post = require("../models/Post");
const Collective = require("../models/Collective");

// @desc    Get all votes
// @route   GET /api/vote-service/v1/votes
// @access  Public
exports.getVotes = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

// @desc    Get all votes for an item
// @route   GET /api/vote-service/v1/votes/:id/votes
// @access  Public
exports.getVotesForItem = asyncHandler(async (req, res, next) => {
  const allVotes = await Vote.find();
  const users = await User.find();
  const collectives = await Collective.find();
  const posts = await Post.find().populate({
    path: "comments",
    select: "text",
  });
  const comments = await Comment.find();
  const votes = allVotes.filter(
    (x) => x.itemID.toString() === req.params.id.toString()
  );
  if (!votes || votes.length < 1) {
    const votesByUser = allVotes.filter(
      (x) => x.userID.toString() === req.params.id.toString()
    );

    if (!votesByUser) {
      return next(
        new ErrorResponse(
          `No votes with field values matching ${req.params.id} found`,
          404
        )
      );
    }

    let votes2 = JSON.parse(JSON.stringify({ data: votesByUser }));

    for (let i = 0; i < votes2.data.length; i++) {
      let item;
      let postItem;
      let collective;
      let isPost = false;

      // create new arr of all data for relevant votes
      let voted = allVotes.filter(
        (x) => x.itemID.toString() === votes2.data[i].itemID.toString()
      );

      // create new arr of the original-posting-user's data for each vote itemID
      let usersVoted = voted.map((x) => {
        let username = users.filter(
          (y) => y._id.toString() === x.userID.toString()
        )[0].username;
        let uid = users.filter(
          (y) => y._id.toString() === x.userID.toString()
        )[0]._id;
        x.username = username;

        return {
          uid: uid,
          username: username,
          itemID: votes2.data[i].itemID,
          id: x._id,
        };
      });

      // get the count of all votes for a specific item
      let favoritesCount = allVotes.filter(
        (x) => x.itemID.toString() === votes2.data[i].itemID.toString()
      ).length;

      // check if the item is a post or comment
      if (votes2.data[i].isPost === true) {
        isPost = true;

        item = posts.filter(
          (x) => x._id.toString() === votes2.data[i].itemID.toString()
        )[0];

        if (item) {
          // console.log(item);
          collective = collectives.filter((x) => {
            return x._id.toString() === item.collective.toString();
          })[0];
        }
      } else {
        isPost = false;
        item = comments.filter(
          (x) => x._id.toString() === votes2.data[i].itemID.toString()
        )[0];
        if (item) {
          postItem = posts.filter(
            (x) => x._id.toString() === item.post.toString()
          )[0];
          if (postItem) {
            // console.log(postItem);
            collective = collectives.filter((x) => {
              return x._id.toString() === postItem.collective._id.toString();
            })[0];
          }
        }
      }
      let userOP = [];
      let UID;

      let addData = {};

      if (votes2.data[i].isPost === true) {
        addData = {
          collective: collective ? collective : null,
          post: isPost ? (item ? item : null) : postItem ? postItem : null,
          comment: isPost ? null : item ? item : null,
          favoritesCount,
          usersVoted,
        };
        if (addData !== null) {
          if (addData.post !== null) {
            UID = addData.post.user;
          }
        }
      } else {
        addData = {
          collective: collective ? collective : null,
          post: isPost ? (item ? item : null) : postItem ? postItem : null,
          comment: isPost ? null : item ? item : null,
          favoritesCount,
          usersVoted,
        };
        if (addData !== null) {
          if (addData.comment !== null) {
            UID = addData.comment.user;
          }
        }
      }
      if (UID) {
        userOP = users.filter((z) => z._id.toString() === UID.toString());
      }
      let addUDATA = {
        userOP: userOP ? userOP : null,
      };
      votes2.data[i] = {
        ...votes2.data[i],
        ...addData,
        ...addUDATA,
      };
    }
    res.status(200).json({
      success: true,
      data: votes2.data,
    });
  } else {
    res.status(200).json({
      success: true,
      data: votes,
    });
  }
});

// @desc    Get single vote
// @route   GET /api/vote-service/v1/votes/:id
// @access  Public
exports.getVote = asyncHandler(async (req, res, next) => {
  const vote = await Vote.findById(req.params.id);

  if (!vote) {
    return next(
      new ErrorResponse(`Vote not found with id of ${req.params.id}`, 404)
    );
  }

  res.status(200).json({
    success: true,
    data: vote,
  });
});

// @desc    Get single vote count
// @route   GET /api/vote-service/v1/votes/:id/getCount
// @access  Public
exports.getCount = asyncHandler(async (req, res, next) => {
  const votes = await Vote.find({itemID: req.params.id});

  if (!votes) {
    // return next(
    //   new ErrorResponse(`Votes not found matching itemID ${req.params.id}`, 404)
    // );
    res.status(200).json({
      success: true,
      data: 0,
    });
  }
  const votesLength = votes.length;
  let positiveCount = 0;
  let negativeCount = 0;

  votes.forEach(x => {
    if (x.tag[0].toString() === 'Positive') positiveCount++;
    if (x.tag[0].toString() === 'Negative') negativeCount++;
  });
  let voteTotal = positiveCount - negativeCount;


  res.status(200).json({
    success: true,
    data: voteTotal,
  });
});

// @desc    Create new vote
// @route   POST /api/vote-service/v1/votes
// @access  Private
exports.createVote = asyncHandler(async (req, res, next) => {
  req.body.userID = req.user.id;
  if (!req.user.id) {
    return next(new ErrorResponse(`Must be a user to vote`, 400));
  }
  const publishedVote = await Vote.findOne({
    userID: req.user.id,
    itemID: req.body.itemID,
  });
  if (publishedVote) {
    console.log(req.body.tag);
    console.log(publishedVote.tag);
    if (publishedVote.tag[0].toString() === req.body.tag[0].toString()) {
      console.log("match");
      publishedVote.remove();
      res.status(200).json({
        success: true,
      });
    } else if (publishedVote.tag[0].toString() !== req.body.tag[0].toString()) {
      console.log("no match");
      publishedVote.remove();
      const newVote = await Vote.create(req.body);
      res.status(201).json({
        success: true,
        data: newVote
      });
    }
  } else {
    console.log("else");
    const newVote = await Vote.create(req.body);
    res.status(201).json({
      success: true,
      data: newVote
    });
  }
  
});

// @desc    Update vote
// @route   PUT /api/vote-service/v1/votes/:id
// @access  Private
exports.updateVote = asyncHandler(async (req, res, next) => {
  let vote = await Vote.findById(req.params.id);

  if (!vote) {
    return next(
      new ErrorResponse(`Collective not found with id of ${req.params.id}`, 404)
    );
  }

  // check if creator and updater are the same user
  if (vote.userID.toString() !== req.user.id) {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to update this vote`,
        401
      )
    );
  }

  vote = await Vote.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    data: vote,
  });
});

// @desc    Delete vote
// @route   DELETE /api/vote-service/v1/votes/:id
// @access  Private
exports.deleteVote = asyncHandler(async (req, res, next) => {
  const vote = await Vote.findById(req.params.id);

  if (!vote) {
    return next(
      new ErrorResponse(`Vote not found with id of ${req.params.id}`, 404)
    );
  }

  // check if creator and updater are the same user
  if (vote.userID.toString() !== req.user.id) {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to delete this vote`,
        401
      )
    );
  }

  vote.remove();

  res.status(200).json({ success: true, data: {} });
});
