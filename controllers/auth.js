const crypto = require("crypto");
const nodemailer = require("nodemailer");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const sendEmail = require("../utils/sendEmail");
const Collective = require("../models/Collective");
const User = require("../models/User");
const Mod = require("../models/Mod");

const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client(process.env.CLIENT_ID);
async function verifyTK(token) {
  const ticket = await client.verifyIdToken({
      idToken: token,
      audience: process.env.CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
      // Or, if multiple clients access the backend:
      //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
  });
  const payload = ticket.getPayload();
  const userid = payload['sub'];
  // If request specified a G Suite domain:
  // const domain = payload['hd'];
  return payload;
}
// verifyTK().catch(console.error);



const randString = () => {
  const length = 6;
  let randStr = "";
  for (let i = 0; i < length; i++) {
    const char = Math.floor(Math.random() * 9 + 1);
    randStr += char;
  }
  return randStr;
};
const sendMail = (
  email,
  verificationString
) => {
  let h2Style = "font-family: sans-serif; color: #ffffff; width: fit-content; padding: 10px; padding-top: 8px; font-weight: 500; background: linear-gradient(-60deg, #c21997 0%, #7719c2 100%) !important; font-size: 1.8em; border-radius: 6px; line-height: 0.9em;";
  let html = `
  <table>
    <tbody>
      <tr>
        <td>
          <h2 style="${h2Style}">srcdoc</h2>
        </td>
      </tr>
      <tr>
        <td>
          <h1>${verificationString}</h1>
        </td>
      </tr>
      <tr>
        <td>
          <p>Click <a href="http://localhost:3000/login"> here </a> to verify your email.</p>
        </td>
      </tr>
    </tbody>
  </table>
  `;
  let subject = "Please confirm your email";


  var Transport = nodemailer.createTransport({
    service: "Gmail",
    auth: {
      user: process.env.FROM_EMAIL,
      pass: process.env.EMAIL_PASS,
    },
  });

  var mailOptions;
  let sender = process.env.FROM_EMAIL;
  mailOptions = {
    from: sender,
    to: email,
    subject,
    html,
  };

  Transport.sendMail(mailOptions, (error, response) => {
    if (error) {
      console.log(error);
    } else {
      console.log("Message Sent");
    }
  });
};

// @desc    Register user
// @route   POST /api/v1/auth/register
// @access  Public
exports.register = asyncHandler(async (req, res, next) => {
  const { username, email, password, role } = req.body;
  const verificationString = randString();

  // Create user
  const user = await User.create({
    verificationString,
    username,
    email,
    password,
    role,
  });

  const verificationTemplate = `

  Verification Code: ${verificationString}
  `

  sendMail(email, verificationTemplate);
  // console.log(user);

  // this will get moved to the verification section
  sendTokenResponse(user, 200, res);
  // await User.findByIdAndDelete(user._id);
  // res.status(200);
  // res.status(200).json({ success: true, data: {} });
});

// @desc    Verify user
// @route   GET /api/v1/auth/verify/:verificationString
// @access  Public
exports.verify = asyncHandler(async (req, res, next) => {
  const { verificationString } = req.params;
  const user = await User.findOne({ verificationString: verificationString });
  if (!user) {
    return next(
      new ErrorResponse(
        `Incorrect code. Please try again.`,
        401
      )
    );
  }
  user.active = true;
  await user.save();
  // res.redirect("/");
  sendTokenResponse(user, 200, res);
});

// @desc    Login user
// @route   POST /api/v1/auth/login
// @access  Public
exports.login = asyncHandler(async (req, res, next) => {
  console.log(req?.body?.gCred);
  if (req?.body?.gCred) {
    let tk = await verifyTK(req?.body?.gCred);

    if (tk && tk.email_verified) {
      const email = tk.email
      const user = await User.findOne({ email });

      if (!user) {
        return next(new ErrorResponse(`Invalid credentials`, 401));
      }

      sendTokenResponse(user, 200, res, req.body.gCred);
    }
  } else {

    const { email, password } = req.body;

    // Validate email & password
    if (!email || !password) {
      return next(new ErrorResponse(`Please provide an email and password`, 400));
    }

    // Check for user
    const user = await User.findOne({ email }).select("+password");

    if (!user) {
      return next(new ErrorResponse(`Invalid credentials`, 401));
    }

    // Check if password matches
    const isMatch = await user.matchPassword(password);

    if (!isMatch) {
      return next(new ErrorResponse(`Invalid credentials`, 401));
    }
    sendTokenResponse(user, 200, res);
  }






  res.status(200).json({
    success: true,
    data: {},
  });
});

// @desc    Log user out/ clear cookie
// @route   GET /api/v1/auth/logout
// @access  Private
exports.logout = asyncHandler(async (req, res, next) => {
  // Date.now() + 10 * 1000
  res.cookie("jwt", "deleted", {
    path: '/',
    expires: new Date('Thu, 01 Jan 1970 00:00:00 GMT'),
    httpOnly: true,
  });

  res.status(200).json({
    success: true,
    data: {},
  });
});

// @desc    Get current logged in user
// @route   GET /api/v1/auth/me
// @access  Private
exports.getMe = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.user.id);
  const collectives = await Collective.find();
  let mods;
  let mods2;
  let modMap;
  let user2;
  try {
    mods = await Mod.find({ userID: req.user.id });
  } catch (error) {
    console.error(error);
  }
  if (mods.length >= 1) {
    mods2 = JSON.parse(JSON.stringify(mods));

    modMap = mods2.map((x) => {
      let cid = x.collectiveID;

      const cMap = collectives.filter((y) => {
        if (cid === y.id) {
          return y.name;
        }
      });
      let cInfo = JSON.parse(JSON.stringify({ data: cMap })).data[0];
      let collectiveName = cInfo.name;
      return { ...x, collectiveName };
    });
    user2 = { user, modMap };
  }

  if (user2) {
    res.status(200).json({
      success: true,
      data: user2,
    });
  } else {
    res.status(200).json({
      success: true,
      data: user,
    });
  }
});

// @desc    Update user details
// @route   PUT /api/v1/auth/updatedetails
// @access  Private
exports.updateDetails = asyncHandler(async (req, res, next) => {
  const fieldsToUpdate = {
    name: req.body.name,
    email: req.body.email,
  };

  const user = await User.findByIdAndUpdate(req.user.id, fieldsToUpdate, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    data: user,
  });
});

// @desc    Update password
// @route   PUT /api/v1/auth/updatepassword
// @access  Private
exports.updatePassword = asyncHandler(async (req, res, next) => {
  // find current logged in user id
  // We also want the password, which by default is select: false
  const user = await User.findById(req.user.id).select("+password");

  // Check if password matches
  if (!(await user.matchPassword(req.body.currentPassword))) {
    return next(new ErrorResponse(`Invalid current password`, 401));
  }

  // given the current pass is a match go ahead and update it with the updated value
  user.password = req.body.updatedPassword;
  await user.save();
  // sendTokenResponse(user, 200, res);
  return res.status(200).json({
    success: true,
    data: "Password Updated.",
  });
});

// @desc    Reset Password
// @route   PUT /api/v1/auth/resetpassword/:resettoken
// @access  Private
exports.resetPassword = asyncHandler(async (req, res, next) => {
  // Get hashed token with crypto
  const resetPasswordToken = crypto
    .createHash("sha256")
    .update(req.params.resettoken)
    .digest("hex");

  // find user by reset token and only if expiration is greater than now
  let user = await User.findOne({
    resetPasswordToken,
    resetPasswordExpire: { $gt: Date.now() },
  });

  if (!user) {
    next(new ErrorResponse(`Invalid token`, 400));
  }

  // if the user does exist, well set the new password
  user.password = req.body.password;
  user.resetPasswordToken = undefined;
  user.resetPasswordExpire = undefined;

  await user.save();

  sendTokenResponse(user, 200, res);
});

// @desc    Forgot password
// @route   POST /api/v1/auth/forgotpassword
// @access  Public
exports.forgotPassword = asyncHandler(async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email });

  if (!user) {
    return next(new ErrorResponse("There is no user with that email", 404));
  }

  // Get reset token
  const resetToken = user.getResetPasswordToken();

  await user.save({ validateBeforeSave: false });

  // Create reset url
  const resetUrl = `https://omnicollective.dev/resetPassword/${resetToken}`;

  const html = `You got this email because of a pass reset request. Please visit the following link to reset your password: \n\n ${resetUrl}`;

  try {
    sendMail(user.email, html, "Pass reset token");

    // await sendEmail({
    //   email: user.email,
    //   subject: "Pass reset token",
    //   message,
    // });

    res.status(200).json({
      success: true,
      data: "Please check your email.",
    });
  } catch (err) {
    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;
    await user.save({ validateBeforeSave: false });
    return next(new ErrorResponse("Email could not be sent", 500));
  }

  res.status(200).json({
    success: true,
    data: user,
  });
});

// Get token from model, create cookie and send response: HELPER
const sendTokenResponse = (user, statusCode, res, gCred) => {
  // Create jwt token
  const token = user.getSignedJwtToken();

  const options = {
    expires: new Date(
      Date.now() + process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60 * 1000
    ),
    httpOnly: true,
  };

  if (process.env.NODE_ENV === "production") {
    options.secure = true;
  }

  res.status(statusCode).cookie("token", token, options).cookie("gCred", gCred, options).json({
    success: true,
    token,
    gCred
  });
};
