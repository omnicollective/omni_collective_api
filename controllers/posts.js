const ErrorResponse = require("../utils/errorResponse");
const fs = require("fs");
const asyncHandler = require("../middleware/async");
const Collective = require("../models/Collective");
const User = require("../models/User");
const Post = require("../models/Post");
const Vote = require("../models/Vote");
const {Storage} = require('@google-cloud/storage');
const storage = new Storage({
  keyFilename: process.env.GOOGLE_APPLICATION_CREDENTIALS,
});

async function uploadFile(filePath, fileName) {
  if (!filePath) return;

  var thumbBucket = storage.bucket("omni-thumbnails");
  // let success = false;
  var stream = require('stream');
  var bufferStream = new stream.PassThrough();
  try {
    bufferStream.end(Buffer.from(filePath, 'base64'));
  } catch (error) {
    console.log(error);
  }


  let file = thumbBucket.file(`${fileName}.png`);
  bufferStream.pipe(file.createWriteStream({
    metadata: {
      contentType: 'image/png',
      metadata: {
        custom: 'metadata'
      }
    },
    public: true,
    validation: "md5"
  }))
  .on('error', function(err) {
    console.log(err);
  })
  .on('finish', function() {
    // The file upload is complete.
    console.log("Upload Complete");
    // success = true;
  });
  

  // await storage.bucket("omni-thumbnails").upload(file, {
  //   destination: destFileName,
  // });

  console.log(`file uploaded to omni-thumbnails`);
  // let fileName = success ? destFileName : "";
  // return fileName;
}


uploadFile().catch(console.error);


// @desc    Get all posts for a user 
// @route   GET /api/v1/users/:userId/posts
// @access  Public
exports.getUserPosts = asyncHandler(async (req, res, next) => {
  const users = await User.find();
  const collectives = await Collective.find();
  const votes = await Vote.find();
  const user = await User.findById(req.params.userId);
  const postsAll = await Post.find();

  if (!user) {
    return res.status(400).json({ success: false });
  }


  const posts = JSON.parse(JSON.stringify(postsAll));

  const postMap = posts.map((x) => {
    let voteCount = [];
    let usersVoted = [];
    let favoritesCount = 0;
    let uid = x.user;
    let cid = x.collective;
    let pid = x.id;

    const userMap = users.filter((y) => {
          if (uid.toString() === y.id.toString()) {
            return y.username;
          }
    });

    let userInfo = JSON.parse(JSON.stringify(userMap))[0];
    let username = "deleted";

    if (userInfo && userInfo.hasOwnProperty('username')) {
      username = userInfo.username;
    }

    const cMap = collectives.filter((y) => {
      if (cid.toString() === y.id.toString()) {
        return y.name;
      }
    });

    let cInfo = JSON.parse(JSON.stringify(cMap))[0];
    let collectiveName = cInfo.name;
    const vMap = votes.filter((y) => {

      if (pid.toString() === y.itemID.toString() && y.tag[0] === "Positive") {
        favoritesCount = voteCount.push("...");
        const userID = y.userID;
        const ID = y.id;
        usersVoted.push({ uid: userID, id: ID });
        return y.id;
      }

    });

    let newX = JSON.parse(JSON.stringify(x));

    return { ...newX, username, collectiveName, favoritesCount, usersVoted };
  });

  let userFiltered = postMap.filter(x => x.user.toString() === user.id.toString());
  res.status(200).json({
    success: true,
    data: userFiltered
  });
});



// @desc    Get posts
// @route   GET /api/v1/posts
// @route   GET /api/v1/collectives/:collectiveId/posts
// @access  Public
exports.getPosts = asyncHandler(async (req, res, next) => {
  const users = await User.find();
  const collectives = await Collective.find();
  const votes = await Vote.find();
  let posts;
  let CIDARR;
  try {
    CIDARR = JSON.parse(req.params.collectiveId);
  } catch (error) {}

  if (CIDARR) {
    posts = await Post.find({
      collective: { $in: CIDARR },
    }).populate({
      path: "comments",
      select: "text",
    });

    const posts2 = JSON.parse(JSON.stringify(posts));
    const postMap = posts2.map((x) => {
      let voteCount = [];
      let usersVoted = [];
      let favoritesCount = 0;
      let uid = x.user;
      let cid = x.collective;
      let pid = x.id;

      const userMap = users.filter((y) => {
        if (uid === y.id) {
          return y.username;
        }
      });
      let userInfo = JSON.parse(JSON.stringify({ data: userMap })).data[0];
      // let username = userInfo && userInfo.username ? userInfo.username : "placeholder";
      let username = "deleted";
        if (userInfo && userInfo.hasOwnProperty('username')) {
          username = userInfo.username;
        }
      const cMap = collectives.filter((y) => {
        if (cid === y.id) {
          return y.name;
        }
      });
      let cInfo = JSON.parse(JSON.stringify({ data: cMap })).data[0];
      let collectiveName = cInfo.name;
      const vMap = votes.filter((y) => {
        if (pid === y.itemID && y.tag[0] === "Positive") {
          favoritesCount = voteCount.push("...");
          const userID = y.userID;
          const ID = y.id;
          usersVoted.push({ uid: userID, id: ID });
          return y.id;
        }
      });
      return { ...x, username, collectiveName, favoritesCount, usersVoted };
    });
    res.status(200).json({
      success: true,
      data: postMap,
    });
  } else {
    if (req.params.collectiveId) {
      posts = await Post.find({ collective: req.params.collectiveId }).populate(
        {
          path: "comments",
          select: "text",
        }
      );
      const posts2 = JSON.parse(JSON.stringify(posts));
      const postMap = posts2.map((x) => {
        let voteCount = [];
        let usersVoted = [];
        let favoritesCount = 0;
        let uid = x.user;
        let cid = x.collective;
        let pid = x.id;

        const userMap = users.filter((y) => {
          if (uid === y.id) {
            return y.username;
          }
        });
        let userInfo = JSON.parse(JSON.stringify({ data: userMap })).data[0];
        // let username = userInfo && userInfo.username ? userInfo.username : "placeholder";
        let username = "deleted";
        if (userInfo && userInfo.hasOwnProperty('username')) {
          username = userInfo.username;
        }
        const cMap = collectives.filter((y) => {
          if (cid === y.id) {
            return y.name;
          }
        });
        let cInfo = JSON.parse(JSON.stringify({ data: cMap })).data[0];
        let collectiveName = cInfo.name;
        const vMap = votes.filter((y) => {
          if (pid === y.itemID && y.tag[0] === "Positive") {
            favoritesCount = voteCount.push("...");
            const userID = y.userID;
            const ID = y.id;
            usersVoted.push({ uid: userID, id: ID });
            return y.id;
          }
        });
        return { ...x, username, collectiveName, favoritesCount, usersVoted };
      });
      res.status(200).json({
        success: true,
        data: postMap,
      });
    } else {
      const posts = JSON.parse(JSON.stringify(res.advancedResults));
      const postMap = posts.data.map((x) => {
        let voteCount = [];
        let usersVoted = [];
        let favoritesCount = 0;
        let uid = x.user;
        let cid = x.collective;
        let pid = x.id;

        const userMap = users.filter((y) => {
          if (uid === y.id) {
            return y.username;
          }
        });
        let userInfo = JSON.parse(JSON.stringify({ data: userMap })).data[0];
        let username = "deleted";
        if (userInfo && userInfo.hasOwnProperty('username')) {
          username = userInfo.username;
        }
      // let username = "placeholder";
        const cMap = collectives.filter((y) => {
          if (cid === y.id) {
            return y.name;
          }
        });
        let cInfo = JSON.parse(JSON.stringify({ data: cMap })).data[0];
        let collectiveName = cInfo.name;
        const vMap = votes.filter((y) => {
          if (pid === y.itemID && y.tag[0] === "Positive") {
            favoritesCount = voteCount.push("...");
            const userID = y.userID;
            const ID = y.id;
            usersVoted.push({ uid: userID, id: ID });
            return y.id;
          }
        });
        return { ...x, username, collectiveName, favoritesCount, usersVoted };
      });
      res.status(200).json({
        success: true,
        data: postMap,
      });
    }
  }
});




// @desc    Get single post
// @route   GET /api/v1/posts/:id
// @access  Public
exports.getPost = asyncHandler(async (req, res, next) => {
  // get collective name here via collective field found in the post model
  // attach to the post obj

  let post = await Post.findById(req.params.id).populate({
    path: "collective",
    select: "name description",
  });

  if (!post) {
    return next(
      new ErrorResponse(`No post with the id of ${req.params.id}`, 404)
    );
  }

  const votes = await Vote.find({ itemID: post._id }).exec();
  // console.log(votes);

  // if (!collective) {
  //   return next(
  //     new ErrorResponse(`No collective with the id of ${post.collective}`, 404)
  //   );
  // }

  let newPostOBJ = { post, votes };

  res.status(200).json({
    success: true,
    data: post,
    votes,
  });
});

// @desc    Add post
// @route   POST /api/v1/collectives/:collectiveId/posts
// @access  Private
exports.addPost = asyncHandler(async (req, res, next) => {
  // console.log(req.body);
  req.body.collective = req.params.collectiveId;
  req.body.user = req.user.id;
  if (req.body.selectedTag) {
    console.log(req.body.selectedTag);
    let tagVal = req.body.selectedTag.text;
    req.body.tag = tagVal ? tagVal : "";
  }

  const currentDate = new Date();
  const date = currentDate.getDate();
  const year = currentDate.getFullYear();
  const month = currentDate.getMonth();
  const destFileName = `${year}-${month}-${date}-${currentDate.getTime()}`;

  if (req.body.photo) {

    uploadFile(req.body.photo, destFileName);
    
  }
  console.log(destFileName);

  req.body.photo = destFileName;

  const collective = await Collective.findById(req.params.collectiveId);

  if (!collective) {
    return next(
      new ErrorResponse(
        `No collective with the id of ${req.params.collectiveId}`,
        404
      )
    );
  }

  const post = await Post.create(req.body);

  // let msg = req.body.photo ? "Found Photo" : "No Photo";

  res.status(200).json({
    success: true,
    data: post,
  });

  // res.status(200).json({
  //   success: true,
  //   data: msg,
  // });
});

// @desc    Update post
// @route   PUT /api/v1/posts/:id
// @access  Private
exports.updatePost = asyncHandler(async (req, res, next) => {
  let post = await Post.findById(req.params.id);
  // console.log(req.params.id);

  // if (req && req.body) console.log(req.body);
  if (!post) {
    return next(
      new ErrorResponse(`No post with the id of ${req.params.id}`, 404)
    );
  }

  // // Make sure user is post owner
  if (post.user.toString() !== req.user.id) {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to update post ${post._id}`,
        401
      )
    );
  }
  let oldPhoto = post.photo;
  const currentDate = new Date();
  const date = currentDate.getDate();
  const year = currentDate.getFullYear();
  const month = currentDate.getMonth();
  const destFileName = `${year}-${month}-${date}-${currentDate.getTime()}`;

  if (req.body.photo) {
    uploadFile(req.body.photo, destFileName);
    
  }
  console.log(destFileName);

  req.body.photo = destFileName;

  post = await Post.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    data: post,
  });
});

// @desc    Delete post
// @route   DELETE /api/v1/posts/:id
// @access  Private
exports.deletePost = asyncHandler(async (req, res, next) => {
  const post = await Post.findById(req.params.id);

  if (!post) {
    return next(
      new ErrorResponse(`No post with the id of ${req.params.id}`, 404)
    );
  }

  // Make sure user is post owner
  if (post.user.toString() !== req.user.id && req.user.role !== "admin") {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to delete post ${post._id}`,
        401
      )
    );
  }

  await post.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});
