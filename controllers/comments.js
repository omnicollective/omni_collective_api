const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const Comment = require("../models/Comment");
const User = require("../models/User");
const Post = require("../models/Post");
const Collective = require("../models/Collective");
const Vote = require("../models/Vote");
const Notification = require("../models/Notification");
const Project = require("../models/Project");

// @desc    Get all comments for a post
// @route   GET /api/v1/comments
// @route   GET /api/v1/posts/:postId/comments
// @access  Public
exports.getComments = asyncHandler(async (req, res, next) => {
  const votes = await Vote.find();
  const users = await User.find();

  if (req.params.postId) {
    const comments = await Comment.find({ post: req.params.postId }).populate({
      path: "children",
      select: "tag text createdAt post user parentId",
    });
    let comments2 = JSON.parse(JSON.stringify({ data: comments }));

    let c2len = comments2.data.length;
    let commentsMap = [];
    let user;
    let usersLen = users.length;
    for (let i = 0; i < c2len; i++) {
      for (let iter = 0; iter < usersLen; iter++) {
        if (users[iter].id === comments2.data[i].user) user = users[iter];
      }

      if (user) {
        const userCP = JSON.parse(JSON.stringify({ data: user }));
        let username = userCP.data.username;
        let photo = userCP.data.photo;
  
        let addUN = {
          username: username ? username : "ERROR FINDING USER",
          photo: photo ? photo : "ERROR FINDING USER",
        };
        comments2.data[i] = { ...comments2.data[i], ...addUN };
      }
      // const fUser = users.filter(x => x.id === comments2.data[i].user)[0];
      // const user = await User.findById(comments2.data[i].user);

    }

    let voteCount = [];
    let usersVoted = [];
    let favoritesCount = 0;
    let voteLength =  votes.length;
    let commentsData = comments2.data;

    for (let i = 0; i < c2len; i++) {
      let uid = commentsData[i].user;
      let cid = commentsData[i].id;
      for (let y = 0; y < voteLength; y++) {
        if (cid === votes[y].itemID) {
          favoritesCount = voteCount.push("...");
          usersVoted.push({ "uid": votes[y].userID, "id": votes[y].id });
        }
      }
      commentsMap = [...commentsMap, { ...commentsData[i], favoritesCount, usersVoted }];
    }

    return res.status(200).json({
      success: true,
      count: commentsMap.length,
      data: commentsMap,
    });

  } else {
    res.status(200).json(res.advancedResults);
  }
});

// @desc    Get all comments for a user
// @route   GET /api/v1/users/:userId/comments
// @access  Public
exports.getCommentsForUser = asyncHandler(async (req, res, next) => {
  if (req.params.userId) {
    const comments = await Comment.find({ user: req.params.userId });

    let comments2 = JSON.parse(JSON.stringify({ data: comments }));
    for (let i = 0; i < comments2.data.length; i++) {
      const post = await Post.findById(comments2.data[i].post);
      let collective = JSON.parse(JSON.stringify({ data: post })).data
        .collective;
      const col = await Collective.findById(collective);
      let addCL = {
        collective: collective ? collective : "ERROR FINDING COLLECTIVE",
        postTitle: post ? post.title : "No Post Found",
      };
      let addCLNAME = {
        collectiveName: col ? col.name : "No Collective Found",
      };
      comments2.data[i] = { ...comments2.data[i], ...addCL, ...addCLNAME };
    }
    return res.status(200).json({
      success: true,
      count: comments2.length,
      data: comments2.data,
    });
  } else {
    res.status(200).json(res.advancedResults);
  }
});

// @desc    Get single comment
// @route   GET /api/v1/comments/:id
// @access  Public
exports.getComment = asyncHandler(async (req, res, next) => {
  const comment = await Comment.findById(req.params.id).populate({
    path: "post",
    select: "name text",
  });

  if (!comment) {
    return next(
      new ErrorResponse(`No comment with the id of ${req.params.id}`, 404)
    );
  }

  res.status(200).json({
    success: true,
    data: comment,
  });
});









// @desc    Create comment on post
// @route   POST /api/v1/posts/:postId/comments
// @access  Private
exports.createComment = asyncHandler(async (req, res, next) => {
  // console.log(req.body);
  req.body.post = req.params.postId;
  req.body.user = req.user._id;
  // console.log(req.params.postId);
  

  const post = await Post.findById(req.params.postId);

  if (!post) {
    return next(
      new ErrorResponse(`No post with the id of ${req.params.postId}`, 404)
    );
  }

  const comment = await Comment.create(req.body);
  // const comment = "howdy";
  res.status(200).json({
    success: true,
    data: comment,
  });
});
















// @desc    Update comment
// @route   PUT /api/v1/comments/:id
// @access  Private
exports.updateComment = asyncHandler(async (req, res, next) => {
  let comment = await Comment.findById(req.params.id);

  if (!comment) {
    return next(
      new ErrorResponse(`No comment with the id of ${req.params.id}`, 404)
    );
  }

  // Make sure user is comment owner
  if (comment.user.toString() !== req.user.id && req.user.role !== "admin") {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to update comment ${comment._id}`,
        401
      )
    );
  }

  comment = await Comment.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    data: comment,
  });
});

// @desc    Delete comment
// @route   DELETE /api/v1/comments/:id
// @access  Private
exports.deleteComment = asyncHandler(async (req, res, next) => {
  const comment = await Comment.findById(req.params.id);

  if (!comment) {
    return next(
      new ErrorResponse(`No comment with the id of ${req.params.id}`, 404)
    );
  }

  // Make sure user is review owner
  if (comment.user.toString() !== req.user.id && req.user.role !== "admin") {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to delete comment ${comment._id}`,
        401
      )
    );
  }
  const body = {
    user: "000000000000000000000000",
  };

  await Notification.deleteMany({ itemID: req.params.id });
  await Vote.deleteMany({ itemID : req.params.id });
  comment = await Comment.findByIdAndUpdate(req.params.id, body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    data: {},
  });
});
