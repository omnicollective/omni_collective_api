const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const Comment = require("../models/Comment");
const User = require("../models/User");
const Post = require("../models/Post");
const Collective = require("../models/Collective");
const Vote = require("../models/Vote");
const Project = require("../models/Project");
const Notification = require("../models/Notification");

// @desc    Create notification
// @route   POST /api/v1/notifications
// @access  Private
exports.createNotification = asyncHandler(async (req, res, next) => {
  // console.log(req.body);
  const { itemID, tag, notifierID, replyID } = req.body;
  const user1 = await User.findById(notifierID);
  const postARR = await Post.find();
  if (!user1) {
    return next(
      new ErrorResponse(
        `Unable to create Notification, no user with ID of ${notifierID} found.`,
        400
      )
    );
  }
  req.body.notifierUsername = user1.username;
  const post = await Post.findById(itemID);
  if (!post) {
    const comment = await Comment.findById(itemID);
    if (!comment) {
      return next(
        new ErrorResponse(
          `Unable to create Notification, no item with ID of ${itemID} found.`,
          400
        )
      );
    }
    const user2 = await User.findById(comment.user);
    req.body.notifieeID = comment.user;
    req.body.notifieeUsername = user2.username;
    req.body.collectiveID = postARR.filter(
      (x) => x._id.toString() === comment.post.toString()
    )[0].collective;
    const notification = await Notification.create(req.body);
    if (!notification) {
      return next(new ErrorResponse(`Unable to create Notification`, 400));
    }
    res.status(200).json({
      success: true,
      data: notification,
    });
  }
  const user2 = await User.findById(post.user);
  req.body.notifieeID = post.user;
  req.body.notifieeUsername = user2.username;
  const notification = await Notification.create(req.body);
  if (!notification) {
    return next(new ErrorResponse(`Unable to create Notification`, 400));
  }
  res.status(200).json({
    success: true,
    data: notification,
  });
});

// @desc    Get all notifications for a user
// @route   GET /api/v1/users/:userId/notifications
// @access  Public
exports.getNotificationsForUser = asyncHandler(async (req, res, next) => {
  const post = await Post.find();
  const comment = await Comment.find();
  const collective = await Collective.find();
  if (req.params.userId) {
    const notifications = await Notification.find({
      notifieeID: req.params.userId,
    });

    if (!notifications) {
      return next(
        new ErrorResponse(
          `No notifications found for user with the id of ${req.params.userId}`,
          404
        )
      );
    }

    let notifications2 = JSON.parse(JSON.stringify({ data: notifications }));
    for (let i = 0; i < notifications2.data.length; i++) {
      const postFilter = post.filter((x) => {
        return notifications2.data[i].itemID.toString() === x._id.toString();
      });
      if (postFilter.length > 0) {
        let commentDATA;
        if (notifications2.data[i].tag[0] === "comment") {
          commentDATA = comment.filter((x) => {
            return (
              notifications2.data[i].commentID.toString() === x._id.toString()
            );
          });
        }

        let addCL = {
          post: postFilter[0],
          commentDATA: commentDATA ? commentDATA[0] : null,
        };
        notifications2.data[i] = {
          ...notifications2.data[i],
          ...addCL,
        };
      } else {
        const commentFilter = comment.filter((x) => {
          return notifications2.data[i].itemID.toString() === x._id.toString();
        });
        let replyDATA;
        if (notifications2.data[i].tag[0] === "reply") {
          replyDATA = comment.filter((x) => {
            return (
              notifications2.data[i].replyID.toString() === x._id.toString()
            );
          });
        }

        if (commentFilter.length > 0) {
          let addCL = {
            comment: commentFilter[0],
            replyDATA: replyDATA ? replyDATA[0] : null,
          };
          notifications2.data[i] = {
            ...notifications2.data[i],
            ...addCL,
          };
        }
      }
    }

    return res.status(200).json({
      success: true,
      count: notifications2.length,
      data: notifications2,
    });
  } else {
    res.status(200).json(res.advancedResults);
  }
});

// @desc    Update notification
// @route   PUT /api/v1/notificationss
// @access  Private
exports.updateNotifications = asyncHandler(async (req, res, next) => {
  if (req.body) {
    if (!Array.isArray(req.body))
      return next(new ErrorResponse(`Not a list of notifications`, 400));
  }

  const notificationStream = [...req.body];
  let noti;
  let notiARR;

  if (notificationStream.length < 1) {
    return next(new ErrorResponse(`No Notifications to modify`, 400));
  }

  notificationStream.forEach(async (x) => {
    await Notification.findByIdAndUpdate(
      x._id,
      { read: true },
      {
        new: true,
        runValidators: true,
      }
    );
    // notiARR.push(noti);
  });
  // console.log(notiARR);
  // if (!noti) {

  //   return next(new ErrorResponse(`unable to update notifications`, 400));
  // }

  res.status(200).json({
    success: true,
    data: [],
  });
});
