const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const Collective = require("../models/Collective");
const User = require("../models/User");
const Post = require("../models/Post");
const Comment = require("../models/Comment");
const Project = require("../models/Project");

// @desc    Get all projects
// @desc    Get all projects for a post
// @route   GET /api/v1/projects
// @route   GET /api/v1/posts/:postId/projects
// @access  Public
exports.getProjects = asyncHandler(async (req, res, next) => {

  if (req.params.postId) {
    let post;
    const comment = await Comment.findById(req.params.postId);
    if (!comment) {
      post = await Post.findById(req.params.postId);
      if (!post) {
        return next(new ErrorResponse(`Nothing found`, 404));
      }
      let projects = await Project.find({ post: req.params.postId });
      return res.status(200).json({
        success: true,
        count: projects.length,
        data: projects,
      });
    }
    const projects2 = await Project.find({ post: req.params.postId });

    return res.status(200).json({
      success: true,
      count: projects2.length,
      data: projects2,
    });
  } else {
    let projects2 = JSON.parse(JSON.stringify(res.advancedResults));
    for (let i = 0; i < projects2.data.length; i++) {
      const user = await User.findById(projects2.data[i].user);
      let username = JSON.parse(JSON.stringify({ data: user })).data.username;
      let addUN = {
        username: username ? username : "ERROR FINDING USER",
      };
      projects2.data[i] = { ...projects2.data[i], ...addUN };
    }
    res.status(200).json(projects2);
  }
});

// @desc    Get all projects for a comment
// @route   GET /api/v1/comments/:commentId/commentProject
// @access  Public
exports.getProjectsForComment = asyncHandler(async (req, res, next) => {
  const id = req.params.commentId;

  if (id) {

    const comment = await Comment.findById(id);
    if (!comment) {
      return next(new ErrorResponse(`Unable to retrieve projects for comment with id: ${id}.`, 404));
    }
    console.log(comment);
    const projects2 = await Project.find({ comment: id });

    return res.status(200).json({
      success: true,
      count: projects2.length,
      data: projects2,
    });

  }
});


// @desc    Get single project
// @route   GET /api/v1/projects/:id
// @access  Public
exports.getProject = asyncHandler(async (req, res, next) => {
  const project = await Project.findById(req.params.id).populate({
    path: "post",
    select: "title text",
  });

  if (!project) {
    return next(
      new ErrorResponse(`No project with the id of ${req.params.id}`, 404)
    );
  }

  res.status(200).json({
    success: true,
    data: project,
  });
});

// @desc    Create new project
// @route   POST /api/v1/projects
// @access  Private
exports.createProject = asyncHandler(async (req, res, next) => {
  // console.log(req.body);
  req.body.user = req.user._id;
  const user = req.user._id;
  const comment = req.body.comment;
  const post = req.body.PID;
  const pages = req.body.pageData;

  // console.log(pages);
  // console.log(user);
  
  if (req.body.PID) {
    const postOBJ = await Post.findById(req.body.PID);

    if (!postOBJ) {
      return next(
        new ErrorResponse(`No post with the id of ${req.body.PID}`, 404)
      );
    }
  }

  if (req.body.comment) {
    req.body.post = req.body.PID;
    const commentOBJ = await Comment.findById(req.body.comment);
    
    if (!commentOBJ) {
      return next(
        new ErrorResponse(`No comment with the id of ${req.body.comment}`, 404)
      );
    }

    pages.forEach(async x => {
      let title = x.title;
      let pageID = x.id;
      let description = x.text;
      let selectedTag = x.selectedTag;
      let replBody = {
        title,
        files: [
          {
            name: x.html.type,
            source: x.html.source
          },
          {
            name: x.css.type,
            source: x.css.source
          },
          {
            name: x.js.type,
            source: x.js.source
          }
        ],
      };
  
      const data = {
        post,
        comment,
        user,
        title,
        pageID,
        description,
        selectedTag,
        replBody
      }
  
      const newProj = await Project.create(data);
      
      res.status(200).json({
        success: true,
        data: newProj,
      });
      return;
    });


    // const project = await Project.create(req.body);
    // res.status(200).json({
    //   success: true,
    //   data: project,
    // });
  } else {

    pages.forEach(async x => {
      let title = x.title;
      let pageID = x.id;
      let description = x.text;
      let selectedTag = x.selectedTag;
      let replBody = {
        title,
        files: [
          {
            name: x.html.type,
            source: x.html.source
          },
          {
            name: x.css.type,
            source: x.css.source
          },
          {
            name: x.js.type,
            source: x.js.source
          }
        ],
      };
  
      const data = {
        post,
        comment,
        user,
        title,
        pageID,
        description,
        selectedTag,
        replBody
      }
  
  
      // console.log(data);
      const newProj = await Project.create(data);
      // console.log(newProj);
    });
  
    const postToReturn = await Post.findById(req.body.PID);
    res.status(200).json({
      success: true,
      data: postToReturn,
    });
  }

});

// @desc    Update project
// @route   PUT /api/v1/projects/:id
// @access  Private
exports.updateProject = asyncHandler(async (req, res, next) => {
  let project = await Project.findById(req.params.id);

  if (!project) {
    return next(
      new ErrorResponse(`No project with the id of ${req.params.id}`, 404)
    );
  }
  // console.log(req.user.id);
  // console.log(req.body);
  if (project.user.toString() !== req.user.id) {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to update project ${project._id}`,
        401
      )
    );
  }

  project = await Project.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  // res.status(200).json({
  //   success: true,
  //   data: "howdy",
  // });
});

// @desc    Update project batch
// @route   PUT /api/v1/projects
// @access  Private
exports.updateProjectBatch = asyncHandler(async (req, res, next) => {
  let pages = req.body.pageData;
  let pid = req.body.pid;
  let post = await Post.findById(pid);
  // console.log(req.body);
  if (pid) {
    if (!post) {
      return next(
        new ErrorResponse(`No post with the id of ${pid}`, 404)
      );
    }
  }

  if (!pages) {
    res.status(200).json({
      success: false,
      data: "No data received.",
    });
  }

  try {


    pages.forEach(async x => {

      let title = x.pageTitle;
      let pageID = x.id;
      let description = x.text;
      let selectedTag = x.selectedTag;
      let replBody = {
        title,
        files: [
          {
            name: x.html.type,
            source: x.html.source
          },
          {
            name: x.css.type,
            source: x.css.source
          },
          {
            name: x.js.type,
            source: x.js.source
          }
        ],
      };
      // console.log(title);
      // console.log(description);
      const data = {
        title,
        pageID,
        description,
        selectedTag,
        replBody
      };

      let projectItem = await Project.findById(pageID);
  
      if (projectItem.user.toString() !== req.user.id) {
        return next(
          new ErrorResponse(
            `User ${req.user.id} is not authorized to update post page ${projectItem._id}`,
            401
          )
        );
      }
    
      await Project.findByIdAndUpdate(pageID, data, {
        new: false,
        runValidators: true,
      });
    });

    post = await Post.findById(pid);
 
    res.status(200).json({
      success: true,
      data: post,
    });

    // console.log(req.body);


    // res.status(200).json({
    //   success: true,
    //   data: "howdy",
    // });
  } catch (error) {
    
  }

});

// @desc    Delete project
// @route   DELETE /api/v1/project/:id
// @access  Private
exports.deleteProject = asyncHandler(async (req, res, next) => {
  const project = await Project.findById(req.params.id);

  if (!project) {
    return next(
      new ErrorResponse(`No project with the id of ${req.params.id}`, 404)
    );
  }

  if (project.user.toString() !== req.user.id) {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to delete project ${project._id}`,
        401
      )
    );
  }

  await project.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});
