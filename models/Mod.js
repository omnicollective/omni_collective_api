const mongoose = require("mongoose");
const slugify = require("slugify");

const ModSchema = new mongoose.Schema(
  {
    postCount: {
      type: Number,
    },
    commentCount: {
      type: Number,
    },
    pointRank: {
      type: Number,
    },
    joinDate: {
      type: Date,
      default: Date.now,
    },
    daysSinceLastModDate: {
        type: Number,
    },
    online: {
        type: Boolean,
        defaut: false,
    },
    active: {
        type: Boolean,
        defaut: false,
    },
    userID: {
        type: String,
        required: true,
    },
    collectiveID: {
        type: String,
        required: true,
    }
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

// Get slug for collective
// ModSchema.pre("save", function (next) {
//   const collectiveID = this._id;
//   this.slug = slugify(collectiveID.toString());
//   next();
// });

// Cascade delete posts & comments if a collective is deleted
// ModSchema.pre("remove", async function (next) {
//   console.log(`Posts and comments being removed from collective ${this._id}`);
//   await this.model("Post").deleteMany({
//     collective: this._id,
//   });
//   await this.model("Comment").deleteMany({
//     collective: this._id,
//   });
//   next();
// });

// Reverse populate with virtuals for posts
// ModSchema.virtual("posts", {
//   ref: "Post",
//   localField: "_id",
//   foreignField: "collective",
//   justOne: false,
// });

module.exports = mongoose.model("Mod", ModSchema);
