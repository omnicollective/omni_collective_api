const mongoose = require("mongoose");
const slugify = require("slugify");

const NotificationSchema = new mongoose.Schema(
  {
    itemID: {
      type: String,
      required: true,
    },
    read: {
      type: Boolean,
      required: true,
      default: false,
    },
    tag: {
      type: [String],
      enum: ["like", "comment", "reply"],
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
    notifierID: {
      type: String,
      required: true,
    },
    notifierUsername: {
      type: String,
      required: true,
    },
    notifieeID: {
      type: String,
      required: true,
    },
    notifieeUsername: {
      type: String,
      required: true,
    },
    collectiveID: {
      type: String,
      required: false,
    },
    replyID: {
      type: String,
      required: false,
    },
    commentID: {
      type: String,
      required: false,
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

module.exports = mongoose.model("Notification", NotificationSchema);
