const mongoose = require("mongoose");
const slugify = require("slugify");
const Populate = require("../utils/autopopulate");

const PageSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      unique: false,
      trim: true,
      maxlength: [150, "Title cannot be more than 50 characters"],
    },
    text: {
      type: String,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
    post: {
        type: mongoose.Schema.ObjectId,
        ref: "Post",
        required: true,
    },
    user: {
        type: mongoose.Schema.ObjectId,
        ref: "User",
        required: true,
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);


//  PostSchema.virtual("comments", {
//     ref: "Comment",
//     localField: "_id",
//     foreignField: "post",
//     justOne: false,
//   });
// Reverse populate with virtuals for comments
PageSchema.virtual("comments", {
  ref: "Comment",
  localField: "_id",
  foreignField: "post",
  justOne: false,
});
PageSchema.virtual("projects", {
  ref: "Project",
  localField: "_id",
  foreignField: "page",
  justOne: false,
});

// Get slug for post
PostSchema.pre("save", function (next) {
  const postID = this._id;
  this.slug = slugify(postID.toString());
  next();
});

// Cascade delete comments if a post is deleted
PageSchema.pre("remove", async function (next) {
  console.log(`Comments being removed from page ${this._id}`);
  await this.model("Comment").deleteMany({
    page: this._id,
  });
  next();
});

module.exports = mongoose.model("Page", PageSchema);
