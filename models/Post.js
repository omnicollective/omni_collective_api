const mongoose = require("mongoose");
const slugify = require("slugify");
const Populate = require("../utils/autopopulate");

const PostSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: [true, "Please add a title"],
      unique: false,
      trim: true,
      maxlength: [50, "Title cannot be more than 50 characters"],
    },
    slug: String,
    text: {
      type: String,
    },
    tag: {
      type: [String],
      enum: ["Q&A", "Tutorial", "Announcement", "Creation"],
    },
    skillLevel: {
      type: String,
      enum: ["Beginner", "Intermediate", "Advanced"],
    },
    postType: {
      type: String,
      enum: ["Q&A", "Tutorial", "Announcement", "Creation"],
    },
    archiveStatus: {
      type: String,
      enum: ["Archived", "Contested", "Un-Archived"],
    },
    photo: {
      type: String,
      default: "",
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
    collective: {
      type: mongoose.Schema.ObjectId,
      ref: "Collective",
      required: true,
    },
    user: {
      type: mongoose.Schema.ObjectId,
      ref: "User",
      required: true,
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

// Reverse populate with virtuals for comments
PostSchema.virtual("comments", {
  ref: "Comment",
  localField: "_id",
  foreignField: "post",
  justOne: false,
});
PostSchema.virtual("projects", {
  ref: "Project",
  localField: "_id",
  foreignField: "post",
  justOne: false,
});
PostSchema.virtual("pages", {
  ref: "Page",
  localField: "_id",
  foreignField: "post",
  justOne: false,
});

// Get slug for post
PostSchema.pre("save", function (next) {
  const postID = this._id;
  this.slug = slugify(postID.toString());
  next();
});

// Cascade delete comments if a post is deleted
PostSchema.pre("remove", async function (next) {
  console.log(`Comments being removed from post ${this._id}`);
  await this.model("Comment").deleteMany({
    post: this._id,
  });
  await this.model("Project").deleteMany({
    post: this._id,
  });
  next();
});

module.exports = mongoose.model("Post", PostSchema);
