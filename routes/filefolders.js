const express = require("express");
const {
  getFileFolders,
  getFileFolder,
  createFileFolder,
  updateFileFolder,
  deleteFileFolder,
} = require("../controllers/filefolders");

const FileFolder = require("../models/FileFolder");

// Include other resource routers
const fileRouter = require("./files");

const router = express.Router({
  mergeParams: true,
});
const advancedResults = require("../middleware/advancedResults");
const { protect, authorize } = require("../middleware/auth");

// Re-route into other resource routers
router.use("/:filefolderId/files", fileRouter);

router
  .route("/")
  .get(getFileFolders)
  .post(protect, authorize("user", "moderator", "admin"), createFileFolder);

router
  .route("/:id")
  .get(getFileFolder)
  .put(protect, authorize("user", "moderator", "admin"), updateFileFolder)
  .delete(protect, authorize("user", "moderator", "admin"), deleteFileFolder);

module.exports = router;
