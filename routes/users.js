const express = require("express");
const {
  getUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser,
  userPhotoUpload,
} = require("../controllers/users");

const { getCommentsForUser } = require("../controllers/comments");
const { getUserPosts } = require("../controllers/posts");
const { getNotificationsForUser } = require("../controllers/notifications");
const User = require("../models/User");

const commentRouter = require("./comments");
const postsRouter = require("./posts");

const router = express.Router({
  mergeParams: true,
});

const advancedResults = require("../middleware/advancedResults");
const { protect, authorize } = require("../middleware/auth");

// router.use("/:userId/comments", commentRouter);
// router.use("/:userId/userPosts", postsRouter);
router.route("/:userId/userPosts").get(getUserPosts);
router.route("/:userId/comments").get(getCommentsForUser);
router.route("/:userId/notifications").get(getNotificationsForUser);

router
  .route("/")
  .get(advancedResults(User), getUsers)
  .post(protect, authorize("admin"), createUser);

router
  .route("/:id")
  .get(getUser)
  .put(protect, updateUser)
  .delete(protect, authorize("user", "admin"), deleteUser);

module.exports = router;
