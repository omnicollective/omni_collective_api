const express = require("express");
const {
  getVotes,
  getVotesForItem,
  getVote,
  createVote,
  updateVote,
  deleteVote,
  getCount
} = require("../controllers/votes");

const Vote = require("../models/Vote");

const router = express.Router();
const advancedResults = require("../middleware/advancedResults");
const { protect, authorize } = require("../middleware/auth");

router.route("/:id/getCount")
.get(getCount)

// routes that don't require a specific id
router
  .route("/")
  .get(advancedResults(Vote), getVotes)
  .post(protect, authorize("user", "moderator", "admin"), createVote);

// routes that require a specific id
router
  .route("/:id")
  .get(getVote)
  .put(protect, authorize("user", "moderator", "admin"), updateVote)
  .delete(protect, authorize("user", "moderator", "admin"), deleteVote);

router.route("/:id/votes")
.get(getVotesForItem)



module.exports = router;
