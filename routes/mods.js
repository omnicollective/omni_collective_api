const express = require("express");
const {
  joinCollective,
  leaveCollective,
  getMods,
  getModByUID,
  updateMod,
} = require("../controllers/mods");
const Mod = require("../models/Mod");

// Include other resource routers
// const commentRouter = require("./comments");
// const projectRouter = require("./projects");

const router = express.Router({
  mergeParams: true,
});
const advancedResults = require("../middleware/advancedResults");
const { protect, authorize } = require("../middleware/auth");

// Re-route into other resource routers
// router.use("/:postId/comments", commentRouter);
// router.use("/:postId/projects", projectRouter);

router
  .route("/")
  .get(
    advancedResults(Mod),
    getMods
  )
  .post(protect, authorize("user", "moderator", "admin"), joinCollective);

router
  .route("/:id")
  .get(getModByUID)
  .put(protect, authorize("user", "moderator", "admin"), updateMod)
  .delete(protect, authorize("user", "moderator", "admin"), leaveCollective);

module.exports = router;
