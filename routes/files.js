const express = require("express");

const {
    createFile,
    getFiles
} = require("../controllers/files");
const router = express.Router({
    mergeParams: true,
});
const { protect, authorize } = require("../middleware/auth");

router
    .route("/")
    .get(getFiles)
    .post(protect, authorize("user", "moderator", "admin"), createFile);

module.exports = router;
