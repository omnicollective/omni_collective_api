const express = require("express");

const {
    createReply,
    getReplies
} = require("../controllers/replies");
const Comment = require("../models/Comment");
const router = express.Router({
    mergeParams: true,
});
const advancedResults = require("../middleware/advancedResults");
const { protect, authorize } = require("../middleware/auth");

router
    .route("/")
    .get(
        // advancedResults(Comment, {
        //     path: "children",
        //     select: "tag text createdAt post user parentId",
        // }),
        getReplies
    )
    .post(protect, authorize("user", "moderator", "admin"), createReply);

module.exports = router;
