const express = require("express");
const {
  createNotification,
  updateNotifications,
} = require("../controllers/notifications");

const router = express.Router();
const advancedResults = require("../middleware/advancedResults");
const { protect, authorize } = require("../middleware/auth");

// routes that don't require a specific id
router
  .route("/")
  .post(protect, authorize("user", "moderator", "admin"), createNotification)
  .put(protect, authorize("user", "moderator", "admin"), updateNotifications);

// routes that require a specific id
// router
//   .route("/:id")

// .delete(protect, authorize("user", "moderator", "admin"), deleteVote);

// router.route("/:id/votes").get(getVotesForItem);

module.exports = router;
