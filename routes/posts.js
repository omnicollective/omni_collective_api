const User = require("../models/User");
const Notification = require("../models/Notification");
const Mod = require("../models/Mod");
const Comment = require("../models/Comment");
// const Post = require("../models/Post");
const Vote = require("../models/Vote");
const path = require("path");
const asyncHandler = require("../middleware/async");

const express = require("express");
const {
  getPosts,
  getPost,
  addPost,
  updatePost,
  deletePost,
  getUserPosts,
} = require("../controllers/posts");
const Post = require("../models/Post");

// Include other resource routers
const commentRouter = require("./comments");
const projectRouter = require("./projects");

const router = express.Router({
  mergeParams: true,
});
const advancedResults = require("../middleware/advancedResults");
const { protect, authorize } = require("../middleware/auth");

// Re-route into other resource routers
router.use("/:postId/comments", commentRouter);
router.use("/:postId/projects", projectRouter);


try {
  router
    .route("/:userId/posts")
    .get(getUserPosts);
} catch (error) {
  console.log(error);
}


router
  .route("/")
  .get(
    advancedResults(Post, {
      path: "comments",
      select: "text",
    }),
    getPosts
  )
  .post(protect, authorize("user", "moderator", "admin"), addPost);

router
  .route("/:id")
  .get(getPost)
  .put(protect, authorize("user", "moderator", "admin"), updatePost)
  .delete(protect, authorize("user", "moderator", "admin"), deletePost);

module.exports = router;
